//imports
const {catalog, workflow} = require("justo");
const eslint = require("justo.plugin.eslint");
const fs = require("justo.plugin.fs");
const hbs = require("justo.plugin.handlebars");

//catalog
catalog.call("lint", eslint, {
  src: "."
}).title("Lint source code");

catalog.workflow("doc", function(params) {
  const dst = `dist/${params.abbr}`;
  const ed = params.ed;
  const abbr = params.abbr;

  fs.rm({path: dst});
  fs.mkdir({path: dst});

  fs.copy({src: `${abbr}-arch.svg`, dst: `${dst}/${abbr}-arch.svg`});
  hbs.render({model: params, src: "doc/getting-started.es.md", dst: `${dst}/getting-started.es.md`});

  fs.mkdir({path: `${dst}/${abbr}`});
  hbs.render({model: params, src: "doc/xxxws/xxxws.es.md", dst: `${dst}/${abbr}/${abbr}.es.md`});

  workflow("Generating chapter", function(params) {
    const chapter = params.value;
    fs.mkdir({path: `${dst}/${chapter}`});
    hbs.render({model: {ed, abbr}, src: `doc/${chapter}/${chapter}.es.md`, dst: `${dst}/${chapter}/${chapter}.es.md`});
  }).forEach("cache", "e", "epub", "http", "httpMsgr", "job", "log", "plugins", "q", "qpub", "webq")();

}).title("Build doc").forEach(
  {title: "Redis WebServices", params: {ed: "Redis WebServices", abbr: "redisws"}},
  {title: "Couch WebServices", params: {ed: "Couch WebServices", abbr: "couchws"}},
  {title: "Postgres WebServices", params: {ed: "Postgres WebServices", abbr: "postgresws"}}
);

catalog.macro("dflt", [
  {task: fs.rm, params: {path: "dist"}},
  catalog.get("lint"),
  catalog.get("doc"),
]).title("Lint and build");
