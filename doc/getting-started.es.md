# Introducción

**{{model.ed}}** es una plataforma de aplicaciones orientadas a (micro)servicios cuyas principales características son las siguientes:

- Es fácil de aprender y de usar.
- Es *serverless*.
- Se ha desarrollado para la plataforma **Node.js**.
- La aplicación se organiza en servicios, los cuales pueden implementarse individualmente en paquetes **NPM**.
- Cada servicio está formado por un conjunto de recursos, donde se distingue entre carpetas y archivos.
- Los servicios son unidades que implementan una determinada funcionalidad de uno de tres tipos: HTTP, consumidor de colas y publicador de colas.
{{#if (eq model.abbr "redisws")}}
- Trabaja con **PostgreSQL** y **Redis**.
{{else if (eq model.abbr "couchws")}}
- Trabaja con **Couchbase**, **CouchDB**, **PouchDB**, **Redis** y **Kafka**.
{{else}}
- Trabaja con **PostgreSQL**.
{{/if}}
- Permite el desarrollo de aplicaciones webs de tiempo real gracias al uso de colas mediante **WebSocket**.

Sus componentes son los siguientes:

- **{{model.ed}}/HTTP** para el desarrollo de aplicaciones **HTTP**.
- **{{model.ed}}/Q** para el desarrollo de aplicaciones consumidoras de colas externas.
  Mediante este componente se lleva a cabo la comunicación interproceso.
- **{{model.ed}}/E** para el desarrollo de aplicaciones consumidoras de colas internas.
  Mediante este componente se lleva a cabo la comunicación intraproceso.
- **{{model.ed}}/WebQ** para la publicación y el consumo de mensajes de colas mediante **WebSocket**.
- **{{model.ed}}/Job** para la publicación planificada y/o periódica de mensajes en colas.

![Arquitectura de una aplicación desarrollada con {{model.ed}}]({{model.abbr}}-arch.svg)

## Aplicación orientada a servicios

Una **aplicación** (*application*) es un programa que realiza una o más funciones.
Está formado por un conjunto de objetos de acceso a datos y por uno o más servicios.

### Archivo de aplicación

**{{model.ed}}** necesita un objeto con los metadatos de la aplicación.
Este objeto puede definirse en un archivo **JavaScript** o **Dogma** o bien indicarse un paquete **NPM**.
Por convenio y buenas prácticas, se utiliza `app.js` o `app.dog`.

El objeto de metadatos debe contener los siguientes datos y debe ser exportado por el módulo:

- `name` (texto), nombre de la aplicación.
- `desc` (texto), descripción de la aplicación.
- `version` (texto), versión de la aplicación.
- `conn` (objeto), declaración de los conectores de datos:
  - `db` (objeto), configuración del objeto de base de datos.
  - `q` (objeto), configuración del objeto de mensajería con el que publicar mensajes en colas externas.
  - `cache` (objeto), configuración del objeto de caché.
  - `logger` (objeto), configuración del objeto de *logging*.
  - `http` (objeto), configuración del mensajero **HTTP**, usado para enviar mensajes **HTTP**.
- `servers` (objeto), servidores de aplicación.
  Se distingue los siguientes tipos:
  - `http` (objeto) para los servicios **HTTP**.
  - `q` (objeto) para los servicios consumidores de colas externas.
  - `e` (objeto) para los servicios consumidores de colas de eventos internos.
  - `job` (objeto) para los servicios de trabajos de publicación periódica de mensajes en colas.
  - `webQ` (objeto) para el servidor de consumo y producción de mensajes de colas mediante **WebSocket**.

He aquí un ejemplo ilustrativo:

```
////////////////
// JavaScript //
////////////////
export default {
  name: "myapp",
  desc: "My test app.",
  version: "1.0.0",
  conn: require("./conn"),
  servers = {
    http: require("./http"),
    q: require("./q"),
    e: require("./e"),
    job: require("./job"),
    webQ: require("./webQ"),
  }
}

#########
# Dogma #
#########
export {
  name = "myapp"
  desc = "My test app."
  version = "1.0.0"
  conn = use("./conn")
  servers = {
    http = use("./http")
    q = use("./q")
    e = use("./e")
    job = use("./job")
    webQ = use("./webQ")
  }
}
```

Para ejecutar una aplicación, utilice el siguiente comando:

```
$ {{model.abbr}} start módulo
```

Ejemplos:

```
$ {{model.abbr}} start ./app.js
$ {{model.abbr}} start móduloGlobal
```

## Servicios

La funcionalidad de una aplicación se divide en servicios, donde un **servicio** (*service*) no es más que un conjunto de recursos que proporcionan una determinada funcionalidad.
En **{{model.ed}}**, se distingue los siguientes tipos de servicios:

- **Servicios HTTP**, aquellos que reaccionan y responden a mensajes **HTTP**.
  Su principal uso es el desarrollo de *APIs RESTs*.
- **Servicios de colas**, aquellos que reaccionan y procesan mensajes recibidos de colas {{#if (eq model.abbr "redisws")}}**Redis**{{else if (eq model.abbr "couchws")}}**Redis** o **Kafka**{{else}}**PostgreSQL**{{/if}}.
  Su principal uso es la ejecución asíncrona de acciones como, por ejemplo, el envío de correos electrónicos, la copia de seguridad, la carga de datos, etc.
- **Servicios de colas de eventos**, aquellos que reaccionan y procesan mensajes remitidos internamente por otros servicios.
  Comunicación intraproceso.
- **Servicios de trabajos**, aquellos que publican periódicamente mensajes en colas.
  Su principal uso es la solicitud de acciones asíncronas como, por ejemplo, el envío de correos electrónicos, la realización de copias de seguridad, etc.
- **Servicios de colas webs**, aquellos que definen colas accesibles mediante **WebSocket**.
  Actúan como un *proxy* de colas de un servicio de mensajería como {{#if (eq model.abbr "redisws")}}**Redis**{{else if (eq model.abbr "couchws")}}**Redis** o **Kafka**{{else}}**PostgreSQL**{{/if}}.
  Lo que facilita el desarrollo de aplicaciones de tiempo real.

### Montaje de servicios

Para que una aplicación pueda utilizar un servicio, debe montarlo.
Para ello, dentro del objeto de servicios de un servidor, cada campo representa un servicio donde su nombre indica el punto de montaje; y el valor, el objeto de metadatos del servicio.
Ejemplo:

```
////////////////
// JavaScript //
////////////////
servers: {
  http: {
    puntoDeMontaje: require("paquete que implementa el servicio"),
    puntoDeMontaje: require("paquete que implementa el servicio"),
    ...
  }

  ...
}

#########
# Dogma #
#########
servers = {
  http = {
    puntoDeMontaje = use("paquete que implementa el servicio")
    puntoDeMontaje = use("paquete que implementa el servicio")
    ...
  }

  ...
}
```

Un **punto de montaje** (*mount point*) es el identificador dentro del servidor de servicios donde se montará el servicio.
Por ejemplo, en el servidor de **HTTP**, la ruta donde montar el servicio y donde se encontrará disponible a los usuarios.
En un servicio de trabajos, el punto de montaje también es importante porque se utiliza para añadir nuevos trabajos dinámicamente.

### Recursos

Un **recurso** (*resource*) es un objeto que implementa una parte de la funcionalidad de un servicio.
Se distingue entre:

- **Carpeta** (*folder*), un contenedor lógico de otros recursos.
- **Archivo** (*file*), una acción u operación.

En **{{model.ed}}**, todo servicio, independientemente de su tipo, se organiza en recursos.
Siendo el recurso **home**, en el que comienza el árbol de recursos del servicio, el cual debe ser una carpeta.

Esta clasificación se ha realizado teniendo en cuenta los conceptos básicos de un sistema de archivos.
Pero se extrapolan a todos los tipos de servicio soportados por **{{model.ed}}**.

En **{{model.ed}}/Q**, un archivo representa un consumidor de cola, o sea, una o más funciones a ejecutar cuando se reciba un mensaje de una determinada cola.
En **{{model.ed}}/Job**, un archivo representa un trabajo, o sea, una operación de publicación periódica de mensajes en una o más colas.
En **{{model.ed}}/WebQ**, un archivo representa una cola accesible mediante **WebSocket**.
Y en **{{model.ed}}/HTTP**, un archivo representa un archivo o método web con el que obtener algo o realizar una determinada operación.

Es importante tener claro cómo trabajar con los recursos, porque se utilizan de manera muy similar en los distintos tipos de servicio.
Ésta es una de las partes más interesantes de **{{model.ed}}**.
No hay que utilizar distintas formas de trabajar para distintos tipos de aplicaciones.
Salvando diferencias debidas a la naturaleza del tipo de servicio, definir un servicio HTTP o de colas es muy similar.
Lo que reduce enormemente la curva de aprendizaje.

Por otra parte, tenemos las acciones. Una **acción** (*action*) no es más que una operación a realizar.
Como, por ejemplo, el envío de un correo electrónico, la publicación de un mensaje, la inserción de un dato, etc.

**{{model.ed}}** utiliza un sistema de procesamiento consistente en una **C invertida**.
Para ello, distingue tres tipos de acciones:

- Las **preacciones** (*pre actions*) o **inicializadores**, aquellas que ejecutan tareas iniciales como, por ejemplo, la autorización o el *log*.
- Las **acciones** (*actions*) propiamente dichas. Realizan la operación que se espera de ellas. La inserción, la actualización, la publicación, etc.
- Las **postacciones** (*post actions*) o **finalizadores**, aquellas que se ejecutan tras la acción.

La C invertida consiste en que cada vez que se accede a un recurso, el procesador trabaja como sigue:

1. Ejecuta las preacciones en el siguiente orden: aplicación, servidor, servicio, carpetas y recurso accedido.
2. Ejecuta las acciones del recurso.
3. Ejecuta las postacciones en el siguiente orden: recurso accedido, carpetas, servicio, servidor y aplicación.

La idea es que las acciones del recurso se dediquen a realizar su función.
Delegando, en las preacciones y las postacciones, la funcionalidad compartida por los recursos.
Así pues, las preacciones son ideales para iniciar el procesamiento, comprobar la autorización y cosas previas a la acción.
Y las postacciones son el lugar donde delegar trabajos como registrar la operación en un *log*, liberar recursos, etc.
Se conoce como C invertida porque va de la aplicación hasta el propio recurso, ejecutando sólo las preacciones; a continuación, se ejecuta las acciones del propio recurso; y finalmente, se vuelve a la aplicación, pero esta vez ejecutando las postacciones.

Todas las acciones se representan mediante funciones escritas en **JavaScript** o **Dogma**, según el lenguaje de programación usado.
Estas funciones pueden presentar distintos parámetros.
He aquí algunos posibles:

- `db`, en este parámetro se inyectará el objeto de bases de datos con el que debe trabajar la función como, por ejemplo, {{#if (eq model.abbr "redisws")}}**PostgreSQL** o **Redis**{{else if (eq model.abbr "couchws")}}**Couchbase**, **CouchDB** o **PouchDB**{{else}}**PostgreSQL**{{/if}}.
- `cache`, aquí se inyectará el objeto de caché, donde mantener datos constantemente accedidos y cuya generación es costosa.
  En nuestro caso, se usa una base de datos {{#if (eq model.abbr "redisws")}}**Redis**{{else if (eq model.abbr "couchws")}}**Redis** o **Couchbase**{{else}}**PostgreSQL**{{/if}}.
- `q`, el sistema de colas donde la función puede publicar mensajes.
  En nuestro caso, se usa {{#if (eq model.abbr "redisws")}}**Redis**{{else if (eq model.abbr "couchws")}}**Redis** o **Kafka**{{else}}**PostgreSQL**{{/if}}.
- `e`, el sistema de colas de eventos, a través del cual comunicar internamente servicios.
- `logger`, el objeto de *logging* como, por ejemplo, la consola o una cola.
- `done`, una función a invocar cuando la operación termine si es asíncrona.
  Cuando no se indica, se asume que la acción es síncrona y finaliza cuando lo hace su cuerpo.

He aquí un ejemplo ilustrativo:

```
////////////////
// JavaScript //
////////////////
(q, done) => {
  q.publish("el mensaje a publicar.", "cola").then(done, done);
}

#########
# Dogma #
#########
fn(q, done)
  q.publish("el mensaje a publicar.", "cola").then(done, done)
end
```

Los parámetros son nominales.
No importa su orden.
Es lo mismo indicar `q` y después `done` que `done` y después `q`.
Sólo hay que recordar el objeto que deseamos, cuál es su nombre e indicarlo en la lista de parámetros de la función.

### Número de ejecuciones máxima

Los recursos se pueden ejecutar un número máximo de veces, permitiendo así añadir uno que deje de ejecutarse alcanzado este valor.
Para fijar este número, se utiliza su campo `times`.
A continuación, un ejemplo de trabajo que se ejecutará como máximo una sola vez:

```
////////////////
// JavaScript //
////////////////
{
  when: "on 31 Dec 2018",
  times: 1,
  publish: {q: "cola", msg: "el mensaje"}
}

#########
# Dogma #
#########
{
  when = "on 31 Dec 2018"
  times = 1
  publish = {q="cola", msg="el mensaje"}
}
```

Pero ojo, los recursos no se suprimen del dispositivo persistente.
Sólo de la memoria.
Son muy utilizados por los trabajos dinámicos.

### Intervalo de ejecución

Los recursos también pueden ejecutarse sólo en un determinado intervalo de tiempo, indicado por los parámetros `begin` y/o `end`.
Son de tipo fecha.
Esto permite activar recursos de manera automática como, por ejemplo, la publicación de artículos, la venta de entradas, la suscripción a eventos, etc.

## Conectores de datos

Un **conector de datos** (*data connector*) representa un objeto de datos como, por ejemplo, una base de datos o un sistema de mensajería.
En **{{model.ed}}**, se utiliza básicamente los siguientes tipos de conexiones de datos:

- `db`, representa una conexión a un determinado motor de bases de datos {{#if (eq model.abbr "redisws")}}**PostgreSQL** o **Redis**{{else if (eq model.abbr "couchws")}}**Couchbase**, **CouchDB** o **PouchDB**{{else}}**PostgreSQL**{{/if}}.
- `q`, representa una conexión a un servidor publicador de mensajes {{#if (eq model.abbr "redisws")}}**Redis**{{else if (eq model.abbr "couchws")}}**Redis** o **Kafka**{{else}}**PostgreSQL**{{/if}}.
- `e`, representa una conexión al servidor de eventos interno.
- `logger`, representa una conexión a un servidor de *logging* como, por ejemplo, un archivo o un sistema de colas {{#if (eq model.abbr "redisws")}}**Redis**{{else if (eq model.abbr "couchws")}}**Redis** o **Kafka**{{else}}**PostgreSQL**{{/if}}.
- `cache`, representa un servidor de caché donde almacenar datos como {{#if (eq model.abbr "redisws")}}**Redis**{{else if (eq model.abbr "couchws")}}**Redis** o **Couchbase**{{else}}**PostgreSQL**{{/if}}.
- `http`, representa un mensajero con el que enviar mensajes **HTTP**, ideal para acceder a APIs RESTs externas.

Toda aplicación puede definir los conectores anteriores a utilizar en las acciones de sus recursos.
Se indican en el objeto `conn` de metadatos:

- De la aplicación.
- Del servidor de servicios.
- Del servicio.

Cuando un recurso utiliza los parámetros `db`, `q`, `logger`, `cache` y `http`, el servidor le inyectará el objeto con el que debe trabajar.
Cuya búsqueda se realizará como sigue:

1. En el servicio que contiene el recurso.
2. En el servidor del servicio.
3. En la aplicación.

Así pues, se puede utilizar las mismas conexiones fijándolas a nivel de aplicación o personalizar más detenidamente a nivel de servidor o de servicio.

La apertura y el cierre de las conexiones las realiza **{{model.ed}}**.
No tenemos que abrirlas nosotros mismos explícitamente.
Aunque claro está, habrá que indicar sus propiedades de conexión en los campos `db`, `cache`, `q`, `logger` y `http` del `conn` correspondiente.

### Conector db

El conector `db` se utiliza para referenciar a una base de datos persistente.
En estos momentos, las opciones posibles son:

{{#if (eq model.abbr "redisws")}}
- `#postgresql`
- `#redis`
{{else if (eq model.abbr "couchws")}}
- `#couchbase`
- `#couchdb`
- `#pouchdb`
{{else}}
- `#postgresql`
{{/if}}

El tipo de *driver* a usar se indica en el campo `impl`, usándose {{#if (eq model.abbr "redisws")}}`#redis`{{else if (eq model.abbr "couchws")}}`#couchbase`{{else}}`#postgresql`{{/if}} si no se indica explícitamente.
El resto de campos contienen los parámetros o propiedades de conexión.

{{#if (eq model.abbr "redisws")}}
Cuando se indica `#redis`, se usa el *driver* `ioredis`, disponiendo de los siguientes campos de conexión:

- `impl` (texto), que siempre será `#redis`.
- `host` (texto), el servidor de **Redis**.
- `port` (número), el puerto donde escucha el servidor.
- `db` (número), la base de datos a conectar.
- `password` (texto), la contraseña de acceso si usa.

Con `#postgresql`, se utiliza `pg`:

- `impl` (texto), siempre `#postgresql`.
- `host` (texto), el servidor de **PostgreSQL**.
- `post` (número), el puerto donde escucha el servidor.
- `db` (texto), la base de datos a conectar.
- `user` (texto), el nombre de la cuenta de usuario.
- `password` (texto), la contraseña de usuario.
{{else if (eq model.abbr "couchws")}}
Con `#couchbase`, se utiliza `couchbase` y disponemos de:

- `impl` (texto), que siempre será `#couchbase`.
- `cluster` (texto), los servidores de **Couchbase**.
- `scheme` (texto), el esquema a utilizar: `http`, `https`, `couchbase` o `couchbases`.
  Predeterminado: `http`.
- `bucket` (texto), el *bucket* al que conectar.
- `user` (texto), el nombre de la cuenta de usuario a usar.
- `password` (texto), la contraseña del usuario.

Con `#couchdb`, se utiliza `nano` y disponemos de:

- `impl` (texto), siempre `#couchdb`.
- `host` (texto), el servidor de **CouchDB**.
- `port` (número), el puerto donde escucha el servidor.
- `db` (texto), el nombre de la base de datos.
- `user` (texto), el nombre de la cuenta de usuario a usar.
- `password` (texto), la contraseña de usuario.

Con `#pouchdb`, se utiliza `pouchdb`:

- `impl` (texto), siempre `#pouchdb`.
- `db` (texto). Archivo local de bases de datos.
  Si no se indica, se creará una base de datos local en memoria.
- `adapter` (texto). Adaptador a usar.
{{else}}
Cuando se indica `#postgresql`, se usa el *driver* `pg`, disponiendo de los siguientes campos de conexión:

- `impl` (texto), siempre `#postgresql`.
- `host` (texto), el servidor de **PostgreSQL**.
- `post` (número), el puerto donde escucha el servidor.
- `db` (texto), la base de datos a conectar.
- `user` (texto), el nombre de la cuenta de usuario.
- `password` (texto), la contraseña de usuario.
{{/if}}

He aquí un ejemplo ilustrativo:

{{#if (eq model.abbr "redisws")}}
```
////////////////
// JavaScript //
////////////////
conn: {
  db: {
    host: "localhost",
    port: 6379,
    db: 0,
    password: "contraseña"
  }
}

#########
# Dogma #
#########
conn = {
  db = {
    host = "localhost"
    port = 6379
    db = 0
    password = "contraseña"
  }
}
```
{{else if (eq model.abbr "couchws")}}
```
////////////////
// JavaScript //
////////////////
conn: {
  db: {
    impl: "#couchbase",
    cluster: "localhost",
    bucket: "db",
    user: "usuario",
    password: "contraseña"
  }
}

#########
# Dogma #
#########
conn = {
  db = {
    impl = "#couchbase"
    cluster = "localhost"
    bucket = "db"
    user = "usuario"
    password = "contraseña"
  }
}
```
{{else}}
```
////////////////
// JavaScript //
////////////////
conn: {
  db: {
    impl: "#postgresql",
    host: "localhost",
    port: 5432,
    db: "db",
    user: "usuario",
    password: "contraseña"
  }
}

#########
# Dogma #
#########
conn = {
  db = {
    impl = "#postgresql"
    host = "localhost"
    port = 5432
    db = "db"
    user = "usuario"
    password = "contraseña"
  }
}
```
{{/if}}

## Cómo continuar

A continuación, se recomienda seguir los siguientes documentos:

1. [{{model.abbr}}]({{model.abbr}}/{{model.abbr}}.es.md), que describe el comando `{{model.abbr}}`.
2. [{{model.ed}}/HTTP](http/http.es.md), que describe más detenidamente cómo desarrollar servicios **HTTP**.
3. [{{model.ed}}/HTTPMsgr](httpMsgr/httpMsgr.es.md), que describe la API del parámetro `http`.
4. [{{model.ed}}/WebQ](webq/webq.es.md), que describe el acceso a colas mediante **WebSocket**.
5. [{{model.ed}}/Plugins](plugins/plugins.es.md), que describe la especificación de *plugins*.
6. [{{model.ed}}/QPub](qpub/qpub.es.md), que describe la API del parámetro `q`.
7. [{{model.ed}}/Q](q/q.es.md), que describe cómo desarrollar servicios (consumidores) de colas.
8. [{{model.ed}}/E](e/e.es.md), que describe cómo desarrollar servicios (consumidores) de colas de eventos internos.
9. [{{model.ed}}/EPub](epub/epub.es.md), que describe la API del parámetro `e`.
10. [{{model.ed}}/Job](job/job.es.md), que describe cómo desarrollar servicios de trabajos que publican mensajes en colas periódicamente.
11. [{{model.ed}}/Cache](cache/cache.es.md), que describe la API del parámetro `cache`.
12. [{{model.ed}}/Log](log/log.es.md), que describe la API del parámetro `logger`.
