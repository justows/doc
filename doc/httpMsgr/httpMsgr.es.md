# HTTPMsgr

**HTTPMsgr** es la especificación del parámetro `http` con el que se puede realizar consultas sencillas a servidores **HTTP** internos y externos.
No es más que un objeto con el que enviar solicitudes **HTTP** y recibir sus respuestas.
Su manera de trabajar es muy similar a la aprendida en el servicio **HTTP** con objeto de reducir la curva de aprendizaje.

Se configura a través del campo `conn`:

- A nivel de aplicación.
- A nivel de servidor de servicios.
- A nivel de servicio.

De manera predeterminada, se configura un `http` a nivel de aplicación si no se indica ninguna configuración explícitamente.

Campos de configuración:

- `domain` (texto). Dominio predeterminado.
  Si en la consulta no se indica uno explícitamente, se utilizará éste.
- `headers` (objeto). Cabeceras predeterminadas.

Ejemplo:

```
////////////////
// JavaScript //
////////////////
conn: {
  http: {
    domain: "http://localhost:8080"
  }
}

#########
# Dogma #
#########
conn = {
  http = {
    domain = "http://localhost:8080"
  }
}
```

## Parámetro http

El parámetro `http` representa un objeto con el que enviar fácilmente solicitudes **HTTP** como, por ejemplo, a APIs RESTs de terceros.
Se puede indicar en las funciones de las acciones.

### Método http.endpoint()

Un **extremo** (*endpoint*) es el recurso al que remitir la solicitud.
A través del método `endpoint()` de `http` se obtiene un objeto con el que remitirle mensajes:

```
////////////////
// JavaScript //
////////////////
http.endpoint(url:string) : Endpoint

#########
# Dogma #
#########
fn http.endpoint(url:text) : Endpoint
```

Si el mensajero (*messenger*) tiene configurado un `domain`, no hace falta indicarlo en su `url`.
Así, por ejemplo, si tenemos como `domain` el valor `http://localhost:8080`, indicar un `url` como `/hello` es similar a `http://localhost:8080/hello`.

Ejemplo:

```
////////////////
// JavaScript //
////////////////
http.endpoint("http://google.es")

#########
# Dogma #
#########
http.endpoint("http://google.es")
```

## Endpoint

El tipo `Endpoint` hace referencia a un recurso al que acceder.
Cada instancia obtenida mediante el método `endpoint()` proporciona varios métodos con los que realizar solicitudes **HTTP** al recurso.

### Parámetro props

Los métodos del tipo `Endpoint` pueden tener un parámetro `props` con el que personalizar el mensaje **HTTP** a remitir:

- `headers` (objeto). Cabeceras a añadir al mensaje.
  Cada cabecera se representa por un campo.
- `contentType` (texto). Valor de la cabecera `Content-Type`.
- `body`. Cuerpo a adjuntar.
  Cuando el `Content-Type` indicado en `headers` o `contentType` sea `application/json`, hay una llamada implícita a `JSON.stringify()` en **JavaScript** o `json.encode()` en **Dogma**, según el lenguaje de programación usado.
  Si el tipo es `x-www-form-urlencoded`, el objeto de datos se convierte implícitamente también.

### Objeto respuesta

Los métodos devuelven una promesa, la cual recibe como parámetro un objeto de tipo respuesta.
Este objeto es muy similar al del parámetro `resp`, con objeto de reducir la curva de aprendizaje.
Sus miembros son:

- `headers : Headers`. Las cabeceras del mensaje.
  Mediante su método `get(name)` se obtiene el valor de una concreta.
- `status() : num`. Devuelve el código de estado.
- `text() : text`. Devuelve el contenido en formato texto si el `Content-Type` es `text/*`.
- `html() : text`. Devuelve el contenido en formato texto si `Content-Type` es `text/html`.
- `json() : map`. Devuelve el contenido en forma de objeto si `Content-Type` es `application/json`.
- `content() : any`. Devuelve el contenido del mensaje.
  Por convenio y buenas prácticas, si sabe cuál es el tipo de contenido, utilice `text()`, `html()` o `json()`.
  Si no lo sabe, utilice `content()`.

### Método Endpoint.get()

Mediante el método `get()`, puede enviarse una solicitud con método **GET**:

```
////////////////
// JavaScript //
////////////////
get(props:object={}) : Promise                    //si no parametrizado
get(params:object, props:object={}) : Promise     //si parametrizado

#########
# Dogma #
#########
fn Endpoint.get(props:={}) : promise              #si no parametrizado
fn Endpoint.get(params:map, props:={}) : promise  #si parametrizado
```

En el *endpoint*, se puede indicar parámetros, cuyos valores se pueden fijar con `params`.
Recordemos que el formato de los parámetros es `&nombre`.

Ejemplo:

```
////////////////
// JavaScript //
////////////////
http.endpoint("/user/&user").get({user: "me"}).then(
  (resp) => {...},
  (err) => {...}
)

#########
# Dogma #
#########
http.endpoint("/user/&user").get({user="me"}).then(
  fn(resp) ... end
  fn(err) ... end
)
```

El ejemplo anterior es similar a:

```
////////////////
// JavaScript //
////////////////
http.endpoint("/user/me").get().then(
  (resp) => {...},
  (err) => {...}
)

#########
# Dogma #
#########
http.endpoint("/user/me").get().then(
  fn(resp) ... end
  fn(err) ... end
)
```

Siempre que se indica una ruta parameterizada en el `endpoint()`, sus métodos deben indicar como primer argumento un objeto con los valores de los parámetros.

### Método Endpoint.head()

El método **HEAD** se utiliza para simular un **GET**:

```
////////////////
// JavaScript //
////////////////
head(props:object={}) : Promise
head(params:object, props:object={}) : Promise

#########
# Dogma #
#########
fn Endpoint.head(props:={}) : promise
fn Endpoint.head(params:map, props:={}) : promise
```

### Método Endpoint.post()

Mediante el método `post()`, se envía un **POST**:

```
////////////////
// JavaScript //
////////////////
post(data, props:object={}) : Promise
post(data, contentType:string, props:object={}) : Promise

#########
# Dogma #
#########
fn Endpoint.post(data:any, props:={}) : promise
fn Endpoint.post(data:any, contentType:text, props:={}) : promise
```

El método `POST` permite enviar nuevos datos al servidor.
Cuando no se indica un `contentType` explícito, se usa `application/json`.

Ejemplo:

```
////////////////
// JavaScript //
////////////////
http.endpoint("user/&user").post({user: "me", password: "contraseña"}).then(
  (resp) => {...},
  (err) => {...}
)


#########
# Dogma #
#########
http.endpoint("user/&user").post({user="me", password="contraseña"}).then(
  fn(resp) ... end
  fn(err) ... end
)
```

Cuando el *endpoint* contiene parámetros, su valor se tomará del objeto de datos pasado al `post()`.
En el ejemplo anterior, el *endpoint* realmente sería: `user/me`.

### Método Endpoint.put()

Mediante `put()`, enviamos un **PUT** al servidor, el cual se suele usar para reemplazar datos:

```
////////////////
// JavaScript //
////////////////
put(data, props:object={}) : Promise
put(data, contentType:string, props:object={}) : Promise

#########
# Dogma #
#########
fn Endpoint.put(data:any, props:={}) : promise
fn Endpoint.put(data:any, contentType:text, props:={}) : promise
```

Cuando el tipo de `data` es un objeto y no se indica un `Content-Type`, se usa `application/json`.

Ejemplo:

```
////////////////
// JavaScript //
////////////////
http.endpoint("user/&user").put({user: "me", password: "contraseña"}).then(
  (resp) => {...},
  (err) => {...}
)

#########
# Dogma #
#########
http.endpoint("user/&user").put({user="me", password="contraseña"}).then(
  fn(resp) ... end
  fn(err) ... end
)
```

### Método Endpoint.patch()

El método `patch` se utiliza para remitir un **PATCH**, el cual se utiliza para reemplazar parte del recurso:

```
////////////////
// JavaScript //
////////////////
patch(data, props:object={}) : Promise
patch(data, contentType:string, props:object={}) : Promise

#########
# Dogma #
#########
fn Endpoint.patch(data:any, props:={}) : promise
fn Endpoint.patch(data:any, contentType:text, props:={}) : promise
```

Cuando el tipo de `data` es un objeto y no se indica un `Content-Type`, se usa `application/json`.

### Método Endpoint.delete()

Si lo que se desea es suprimir el recurso, se envía un **DELETE**, el cual se puede enviar con el método `delete()`:

```
////////////////
// JavaScript //
////////////////
delete(props:object={}) : Promise                     //si no parametrizado
delete(params:object, props:object={}) : Promise      //si parametrizado

#########
# Dogma #
#########
fn Endpoint.delete(props:={}) : promise               #si no parametrizado
fn Endpoint.delete(params:map, props:={}) : promise   #si parametrizado
```

El parámetro `params` se utiliza para proporcionar los valores de los parámetros del *endpoint* indicado.

Ejemplo:

```
////////////////
// JavaScript //
////////////////
http.endpoint("/user/&user").delete({user: "me"}).then(
  (resp) => {...},
  (err) => {...}
)

#########
# Dogma #
#########
http.endpoint("user/&user").delete({user="me"}).then(
  fn(resp) ... end
  fn(err) ... end
)
```
