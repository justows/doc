# {{model.ed}}/Log

**{{model.ed}}/Log** es la especificación del parámetro `logger` de las acciones.
Tiene como objeto describir cómo generar entradas en el *log* del sistema.

Una **entrada de registro** (*log entry*) es un mensaje que informa de algo.
Tiene los siguientes componentes:

- El **nivel** (*level*) que indica la importancia del mensaje: depuración, información, aviso, error o fatal.
- La fecha y hora en que se generó.
- El **origen** (*source*) de la entrada, o sea, quién la generó.
- El texto del mensaje.

## Implementaciones

**{{model.ed}}** puede utilizar un servidor {{#if (eq model.abbr "redisws")}}**Redis**{{else if (eq model.abbr "couchws")}}**Redis** o **Kafka**{{else}}**PostgreSQL**{{/if}} como *logger*.
Cada vez que se genera una entrada de registro, se remite a una cola.
En ella, puede haber cero, uno o más consumidores que lo procesarán de manera independiente.
Por ejemplo, puede haber un consumidor como `{{model.abbr}} log` que muestre el mensaje en la consola y otro que lo escriba en una base de datos con un TTL.

Los mensajes se publicarán en la cola mediante un texto que contiene un objeto en formato **JSON** como el siguiente:

```
{
  "src": "origen",
  "ts": "fecha en formato ISO",
  "level": "DEBUG|INFO|WARN|ERROR|FATAL",
  "msg": "el texto del mensaje"
}
```

Su configuración se realiza en el campo `conn` de la aplicación, de los servidores y/o de los servicios.

{{#if (in model.abbr "redisws" "couchws")}}
### Redis

Para remitir las entradas del registro a una cola **Redis**, use los parámetros de conexión:

- `impl` (texto). Siempre `#redis`.
- `host` (texto)
- `port` (texto)
- `db` (número)
- `q` (texto). Cola en la que publicar las entradas.
- `password` (texto)
- `src` (texto). Origen predeterminado.
{{/if}}

{{#if (eq model.abbr "postgresws")}}
### PostgreSQL

Para remitir las entradas del registro a una cola **PostgreSQL**, use los parámetros de conexión:

- `impl`. Siempre `#postgresql`.
- `host` (texto)
- `port` (texto)
- `db` (texto)
- `q` (texto). Cola en la que publicar las entradas.
- `user` (texto)
- `password` (texto)
- `src` (texto). Origen predeterminado.

{{/if}}

## Parámetro logger

Recordemos que el parámetro `logger` representa el objeto con el generar entradas de registro.
En las acciones, se puede utilizar tanto `logger` como `log`.
Pero en `conn`, debemos usar siempre `logger` para configurarlo.

A la hora de escribir mensajes, hay que indicar el nivel, el texto y el origen.
Se puede utilizar cualquiera de los siguientes métodos:

```
////////////////
// JavaScript //
////////////////
logger.debug(text:string)
logger.debug(src:string, text:string)
logger.info(text:string)
logger.info(src:string, text:string)
logger.warn(text:string)
logger.warn(src:string, text:string)
logger.error(text:string)
logger.error(src:string, text:string)
logger.fatal(text:string)
logger.fatal(src:string, text:string)

#########
# Dogma #
#########
fn logger.debug(text:text)
fn logger.debug(src:text, text:text)
fn logger.info(text:text)
fn logger.info(src:text, text:text)
fn logger.warn(text:text)
fn logger.warn(src:text, text:text)
fn logger.error(text:text)
fn logger.error(src:text, text:text)
fn logger.fatal(text:text)
fn logger.fatal(src:text, text:text)
```

Observe que hay que utilizar un método u otro atendiendo al nivel de importancia del mensaje.
La fecha y la hora lo fijará el propio *logger*.
Si no se indica un origen, se utilizará el predeterminado del `logger`.
