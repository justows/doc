# {{model.ed}}/EPub

**{{model.ed}}/EPub** es la especificación que describe la API utilizada por el parámetro `e` de las acciones.
Tiene como objeto describir cómo publicar eventos mediante este objeto.

A diferencia de otros objetos como `q` o `db`, su conector no es configurable.
Viene configurado de fábrica para publicar en el sistema de mensajería interno.

## Parámetro e

El parámetro `e` de las acciones presenta un único método, `publish()`, similar a `q`, con el que publicar un mensaje en una cola de eventos internos:

```
e.publish(msg:any, queue:text)
```

El método puede leerse como sigue: *publica el mensaje M en la cola C*.

Ejemplo:

```
//JavaScript
(e, done) => {
  e.publish({to: "equipo", text: "Buenos días!!!"}, "email");
}

//Dogma
fn(e, done)
  e.publish({to="equipo", text="Buenos días!!!"}, "email")
end
```
