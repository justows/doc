# {{model.ed}}/Plugins

**{{model.ed}}/Plugins** es la especificación para el desarrollo e integración de *plugins*.
Donde un **plugin** es un componente que define acciones relacionadas con una determinada funcionalidad.
Estas acciones pueden integrarse en cualquier componente como, por ejemplo, la aplicación, un servidor, un servicio o un recurso.

Como ejemplo de *plugin* tenemos el soporte de **JWT** (*JSON Web Token*) utilizable en **{{model.ed}}/HTTP** y **{{model.ed}}/WebQ**.

## Desarrollo de plugins

Un *plugin* se desarrolla mediante un objeto, el cual contiene acciones, las cuales se integrarán cuando se incluya el *plugin* en un componente.
Ejemplo:

```
////////////////
// JavaScript //
////////////////
export default {
  pre: función,
  acción1: función,
  acción2: función,
  post: función
}

#########
# Dogma #
#########
export {
  pre = función
  acción1 = función
  acción2 = función
  post = función
}
```

Para facilitar su reutilización, se puede desarrollar como un paquete publicado en un repositorio como **NPM**.
Por convenio, los *plugins* oficiales tienen el formato `{{model.abbr}}.plugin.nombre` o `justows.plugin.nombre`.
Por favor, no utilice estos formatos para plugins **no** oficiales.

## Inclusión de plugins

Para incluir un *plugin*, no hay más que utilizar el campo `plugins` de los objetos de metadatos.
Ejemplo:

```
////////////////
// JavaScript //
////////////////
servers: {
  http: {
    endpoints: ["http://localhost:8080"],
    plugins: [require("justows.plugin.http.jwt")({alg: "HS384", secret: "abcdef", cookie: "Token"})],
    services: {
      ...
    }
  }
}

#########
# Dogma #
#########
servers = {
  http = {
    endpoints = ["http://localhost:8080"]
    plugins = [use("justows.plugin.http.jwt")({alg="HS384", secret="abcdef", cookie="Token"})]
    services = {
      ...
    }
  }
}
```

En muchas ocasiones, los *plugins* exportan una función en vez de un objeto.
Esta función, tras ser invocada con cero, uno o más argumentos para su inicialización, devuelve el objeto de acciones a incluir en el componente.
Por convenio y buenas prácticas, se recomienda utilizar esta forma de obtener las acciones.
Así, si el día de mañana hay que obtener unas acciones u otras atendiendo a algún argumento, no romperemos su definición.

## Ejemplos de plugins

- [justows.plugin.http.jwt](https://bitbucket.org/justows/justows-plugin-http-jwt).
- [justows.plugin.http.log](https://bitbucket.org/justows/justows-plugin-http-log).
