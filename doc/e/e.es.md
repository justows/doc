# {{model.ed}}/E

**{{model.ed}}/Q** permite trabajar con colas externas.
Mientras que **{{model.ed}}/E** con colas internas, permitiendo así que los servicios de la aplicación puedan comunicarse entre sí fácilmente.

Sus componentes son los siguientes:

- Servidor de colas de eventos.
- Servicios de colas de eventos.
- Consumidores de colas de eventos.

Este servidor, al igual que **{{model.ed}}/Q**, se organiza en servicios y, éstos a su vez, en consumidores y/o carpetas.

## Servidor de colas de eventos

El **servidor de colas de eventos** (*event queue server*) es el componente que se encarga de servir los servicios de colas internas.
Existe uno para toda la aplicación, el cual es compartido por todos los servicios montados, independientemente de su tipo.
Se define mediante el campo `e` del objeto `servers` de la aplicación.
Por convenio y buenas prácticas, se recomienda ubicar su definición en un archivo dedicado:

- `e.js` si estamos usando **JavaScript**.
- `e.dog` cuando se usa **Dogma**.

Ejemplo:

```
////////////////
// JavaScript //
////////////////

// app.js
export default {
  name: "myapp",
  desc: "My test app.",
  version: "1.0.0",
  conn: require("./conn"),
  servers = {
    e: require("./e"),
    ...
  }
}

// e.js
export default {
  conn = {...},
  services: {
    puntoMontaje1: {
      //definición del servicio
    },

    puntoMontaje2: {
      //definición del servicio
    },

    ...
  }
}

#########
# Dogma #
#########

# app.dog
export {
  name = "myapp"
  desc = "My test app."
  version = "1.0.0"
  conn = use("./conn")
  servers = {
    e = use("./e")
    ...
  }
}

# e.dog
export {
  conn = {...}
  services = {
    puntoMontaje1 = {
      #definición del servicio
    }

    puntoMontaje2 = {
      #definición del servicio
    }

    ...
  }
}
```

El objeto de metadatos del servidor contiene los siguientes campos:

- `services` (objeto). Los servicios de colas. Se montan de manera similar a **{{model.ed}}/Q**.
- `conn` (objeto). Si el servidor utiliza conectores específicos, independientes de la aplicación, se pueden definir en su propio `conn`.

## Servicios de colas de eventos

Un **servicio de colas de eventos** (*event queue service*) es aquel que proporciona una determinada funcionalidad basada en consumo de mensajes de colas de eventos.
Se definen exactamente igual que los servicios de colas de **{{model.ed}}/Q**.
Pero cuando definimos una cola, no se utiliza el conector `q` para suscribirse a ella.
Se suscribe a un motor de colas interno.

## Consumidores de colas de eventos

Un **consumidor de colas de eventos** (*event queue consumer*) es un elemento que ejecuta sus acciones cada vez que se recibe un mensaje de una cola de eventos determinada.
Se definen igual que los consumidores de **{{model.ed}}/Q**.

### Acciones

Se definen igual que las acciones de **{{model.ed}}/Q**.
Y pueden tener los siguientes parámetros nominales:

- `db`
- `cache`
- `q`. Conector para publicar mensajes en colas externas.
- `e`. Conector para publicar mensajes en colas de eventos internas.
- `logger`
- `msg`. Mensaje recibido.
- `done`

### Nombre de la cola de eventos

Todo consumidor se asocia a una cola, la cual se puede indicar como nombre del consumidor o bien mediante su campo `q`.
Si se omite `q`, se usará el nombre del consumidor.

### Momento de ejecución

Las acciones del consumidor se ejecutarán asíncronamente cada vez que se publique un mensaje en la cola indicada.
