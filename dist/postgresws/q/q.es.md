# Postgres WebServices/Q

**Postgres WebServices/Q** es un servidor consumidor de mensajes publicados en colas, en nuestro caso, en un servidor de **PostgreSQL**.
Tiene como objeto reaccionar ante mensajes publicados y recibidos de determinadas colas.
A diferencia de **HTTP**, donde el servidor reacciona ante la recepción de mensajes **HTTP**, en **Postgres WebServices/Q** el disparador es un mensaje recibido de una cola.

Sus componentes son los siguientes:

- Servidor de colas.
- Servicios de colas.
- Consumidores de colas.

Este servidor, al igual que los demás, se organiza en servicios y, éstos a su vez, en consumidores y/o carpetas.

## Servidor de colas

El **servidor de colas** (*queue server*) es el componente que se encarga de servir los servicios de colas.
Se define mediante el campo `q` del objeto `servers` de la aplicación.
Por convenio y buenas prácticas, se recomienda ubicar su definición en un archivo dedicado:

- `q.js` si estamos usando **JavaScript**.
- `q.dog` cuando se usa **Dogma**.

Ejemplo:

```
////////////////
// JavaScript //
////////////////

// app.js
export default {
  name: "myapp",
  desc: "My test app.",
  version: "1.0.0",
  conn: require("./conn"),
  servers = {
    q: require("./q"),
    ...
  }
}

// q.js
export default {
  conn = {...},
  services: {
    puntoMontaje1: {
      //definición del servicio
    },

    puntoMontaje2: {
      //definición del servicio
    },

    ...
  }
}

#########
# Dogma #
#########

# app.dog
export {
  name = "myapp"
  desc = "My test app."
  version = "1.0.0"
  conn = use("./conn")
  servers = {
    q = use("./q")
    ...
  }
}

# q.dog
export {
  conn = {...}
  services = {
    puntoMontaje1 = {
      #definición del servicio
    }

    puntoMontaje2 = {
      #definición del servicio
    }

    ...
  }
}
```

El objeto de metadatos del servidor contiene los siguientes campos:

- `services` (objeto). Los servicios de colas.
  Se montan de manera similar al resto de servidores.
- `conn` (objeto). Si el servidor utiliza conectores específicos, independientes de la aplicación, se pueden definir en su propio `conn`.

## Servicios de colas

Un **servicio de colas** (*queue service*) es aquel que implementa o proporciona una determinada funcionalidad basada en consumo de mensajes de colas.
Los servicios consumidores de colas se definen de manera similar a los demás servicios, teniendo en cuenta lo siguiente:

- Todo consumidor es un recurso de tipo archivo.
- Los consumidores cuelgan directa o indirectamente del recurso `home` del servicio.
- Cuando no se indica el campo `q`, se utiliza el nombre del recurso como la cola.
- Si un servicio debe utilizar un servidor de colas distinto del resto, puede definir su propio conector `q`.
- Los servicios se pueden definir como paquetes de **NPM**, lo más común, lo que facilita su reutilización, mantenimiento, desarrollo y prueba.

Vamos a ver un ejemplo de objeto de metadatos de servicio:

```
////////////////
// JavaScript //
////////////////
export default {
  name: "nombre del servicio",
  desc: "descripción del servicio",
  version: "version del servicio",
  conn: { ... },
  home: ...
}

#########
# Dogma #
#########
export {
  name = "nombre del servicio"
  desc = "descripción del servicio"
  version = "version del servicio"
  conn = { ... }
  home = ...
}
```

Los consumidores se suscriben a las colas indicadas del servidor de colas `q`.
Si éste es específico del servicio, sus datos de conexión se deben indicar en el campo `q`, tal como sabemos de los demás servicios.

Recordemos que el recurso `home` representa la raíz del árbol de recursos del servicio.
Cada recurso puede ser un consumidor o una carpeta.
Las carpetas se identifican fácilmente mediante el campo `children` y representan contenedores lógicos de otras carpetas y/o consumidores.
Cuando un recurso no contiene el campo `children`, automáticamente se considera un consumidor.

## Consumidores de colas

Un **consumidor de cola** (*queue consumer*) es un elemento que ejecuta sus acciones cada vez que se recibe un mensaje de una determinada cola.
Puede representar una operación de copia de seguridad, una comprobación de cualquier tipo, una operación de monitorización, etc.
Para solicitar su ejecución se debe publicar un mensaje en la cola correspondiente.

Para ir abriendo boca, he aquí una plantilla de ejemplo de definición de un consumidor:

```
////////////////
// JavaScript //
////////////////
consumidor: {
  q: "cola",
  pre: accionesPrevias,
  consumer: accionesDelConsumidor,
  post: accionesPosteriores
}

#########
# Dogma #
#########
consumidor = {
  q = "cola"
  pre = accionesPrevias
  consumer = accioesDelConsumidor
  post = accionesPosteriores
}
```

No olvidemos que también podemos indicar los campos `time`, `begin` y `end`.

Si la cola sólo dispone de la acción `consumer`, se puede definir como sigue:

```
////////////////
// JavaScript //
////////////////
cola: (msg, db, done) => {
  //...
}

#########
# Dogma #
#########
cola = fn(msg, db, done)
  #...
end
```


### Acciones

Recordemos que las acciones son funciones que contienen la lógica que debe ejecutarse cada vez que se recibe un mensaje de la cola indicada.
Se distingue entre:

- Preacciones o inicializadores, aquellas que se ejecutan antes de las acciones.
  Ideales para comprobaciones previas, autorizaciones, etc.
  Se definen como `pre`, que puede ser una función o una lista de funciones.
- Acciones, aquellas que contienen la lógica propia del consumidor.
  O sea, lo que deseamos se ejecute cuando se reciba el mensaje.
  En el caso de **Postgres WebServices/Q**, se utiliza la acción cuyo nombre es `consumer`.
- Postacciones o finalizadores, aquellas que se ejecutan tras la acción consumidora.
  Se definen como `post`.

Las acciones se definen en **JavaScript** o **Dogma**, según el lenguaje de programación con el que estemos trabajando.
Estas funciones pueden tener los siguientes parámetros nominales:

- `db`. Servidor de bases de datos con el que trabajar.
- `cache`. Servidor de caché con el que trabajar.
- `q`. Servidor de colas con el que publicar mensajes.
- `logger`. Objeto de *logging*.
- `msg`. Mensaje publicado en la cola, el cual es responsable de la ejecución de la acción.
  Los mensajes recibidos por estas colas deben encontrarse en formato **JSON**.
  Y serán convertidos automáticamente al objeto correspondiente mediante las funciones `JSON.parse()` en **JavaScript** y `json.decode()` en **Dogma**.
- `done`. Función a ejecutar cuando la acción finalice su trabajo si estamos ante una acción asíncrona.
  Si no se indica, la acción se considera síncrona y finaliza cuando lo hace su cuerpo.

Recordemos que los parámetros son nominales, no importa su orden.

He aquí un ejemplo ilustrativo:

```
////////////////
// JavaScript //
////////////////
cola: (msg, db, done) => {
  db.upsert(msg.key, msg, done);
}

#########
# Dogma #
#########
cola = fn(msg, db, done)
  db.upsert(msg.key, msg, done)
end
```

### Nombre de la cola

Todo consumidor se asocia a una cola, la cual se puede indicar como nombre del consumidor o bien mediante su campo `q`.
Si se omite `q`, se usará el nombre del consumidor.

### Momento de ejecución

Las acciones del consumidor se ejecutarán cada vez que se publique un mensaje en la cola indicada.
