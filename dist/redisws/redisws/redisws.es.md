# Comando redisws

`redisws` es el comando para trabajar con la plataforma **Redis WebServices**.
Se encuentra disponible en **NPM** mediante su paquete homónimo.

Con él, se puede:

- Ejecutar una aplicación.
- Consumir mensajes de una cola de *log* para mostrar las entradas del registro a medida que se van produciendo.
  Muy útil tanto en desarrollo como en producción.

## Instalación

Para instalar `redisws`, hay que ejecutar:

```
npm i -g redisws
```

Una vez instalado, por convenio y buenas prácticas, se recomienda comprobar que hay acceso al comando `redisws`:

```
$ which redisws
$ redisws help
```

## Comando log

Para facilitar el acceso al registro de eventos de **Redis WebServices** que, recordemos, funciona mediante colas, se puede utilizar el comando `log`.
La ventaja de tener el registro de *logs* en una cola es que podemos leerlo desde cualquier máquina que tenga acceso al servidor de colas.
Además, podemos tener un consumidor para mostrar los mensajes en la consola y otro para almacenarlos persistentemente, por ejemplo, en una base de datos o en un archivo.

Su ayuda se obtiene como sigue:

```
redisws log help
```

Las opciones de conexión son las siguientes:

- `--host`, servidor de colas al que contectar.
- `--port`, puerto del servidor de colas.
- `--password`, contraseña con la que conectar.

La cola predeterminada de la que consumir mensajes es `log`.

Por otra parte, tenemos opciones de filtro con los que limitar los mensajes a mostrar en nuestra consola:

- `--src`, sólo mostrar las entradas del origen indicado.
- `--level`, sólo mostrar las entradas cuyo nivel sea el indicado.
- `--min`, sólo mostrar las entradas cuyo nivel sea mayor o igual al indicado.

De manera predeterminada, los mensajes no muestran el momento en que se generaron.
Si desea hacerlo, indicar la opción `--ts`.

He aquí un ejemplo ilustrativo:

```
$ redisws log -s=HTTP --ts
Subscribed to log!
[2018-04-28T08:11:27.798Z][INFO][HTTP] starting server...
[2018-04-28T08:11:27.803Z][INFO][HTTP] server started on http://localhost:8080.
```

Y ahora, otro que se limita a mostrar los mensajes de error o fatales:

```
$ redisws log --ts --min=ERROR
```

## Comando start

El comando `start` se utiliza para iniciar una aplicación.
Su ayuda se obtiene mediante:

```
redisws start help
```

Ejemplo:

```
$ redisws start ./app
```

## Paradas del proceso

Para parar formalmente un proceso iniciado con `redisws start`:

- Enviar la señal `SIGTERM` al proceso mediante el comando `kill`.
- Usar `Ctrl+C` en la consola, lo que envía la señal `SIGINT`.

En un cierre de este tipo, se detienen los servicios, los servidores y los conectores.
Ejemplo:

```
$ kill -SIGTERM 19697
```

Para simular una caída, o sea, para realizar un cierre informal, puede utilizar la señal `SIGKILL`:

```
$ kill -SIGKILL 20054
```
