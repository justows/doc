# Redis WebServices/Cache

**Redis WebServices/Cache** es la especificación del parámetro `cache` de las acciones.
Tiene como objeto describir cómo acceder a la caché.

## Implementaciones

**Redis WebServices** puede utilizar un servidor **Redis** como caché.

Recordemos que el parámetro `cache` representa el objeto con el acceder a la caché.
Se puede configurar:

- A nivel de aplicación, en su objeto de metadatos.
- A nivel de servidor de servicios.
- A nivel de servicio.


### Caché Redis

La configuración debe contener los parámetros de conexión a un servidor **Redis**:

- `impl` (texto), siempre `#redis`.
- `host` (texto)
- `port` (texto)
- `db` (número)
- `prefix` (texto), prefijo a añadir a las claves.
  De esta manera, no interferirán con el resto de claves de la base de datos.
- `password` (texto)

Ejemplo:

```
////////////////
// JavaScript //
////////////////
cache: {
  impl: "#redis",
  host: "localhost",
  port: 6379,
  db: 0,
  prefix: "cache::"
}

#########
# Dogma #
#########
cache = {
  impl = "#redis"
  host = "localhost"
  port = 6379
  db = 0
  prefix = "cache::"
}
```


## Parámetro cache

El parámetro `cache` representa una base de datos clave-valor donde se almacenan ítems identificados de manera única por una clave.

### Método cache.set()

Mediante el método `set()` se inserta o reemplaza una clave.
Si la clave no existe, la crea; en otro caso, reemplaza su valor.
Su signatura es como sigue:

```
////////////////
// JavaScript //
////////////////
cache.set(key:string, value, opts={}) : Promise

#########
# Dogma #
#########
fn cache.set(key:text, value, opts:={}) : promise
```

Las claves pueden tener un **TTL** (*time-to-live*), esto es, un tiempo de vida, transcurrido el cual se suprimen automáticamente de la base de datos.
Si no se indica, se mantendrán hasta que se supriman explícitamente.
El valor de TTL se indica mediante el campo `ttl`, en segundos, del parámetro `opts`.

He aquí un ejemplo:

```
////////////////
// JavaScript //
////////////////
cache.set("clave", valor, {ttl: 60*60});

#########
# Dogma #
#########
cache.set("clave", valor, {ttl=60*60})
```

El valor se almacena en formato **JSON**.

### Método cache.get()

Mediante `get()` se recupera el valor de una clave si existe:

```
////////////////
// JavaScript //
////////////////
cache.get(key:string) : Promise
cache.get(key:string, opts:object) : Promise

#########
# Dogma #
#########
fn cache.get(key:text) : promise
fn cache.get(key:text, opts:{ttl?:num}) : promise
```

El método devuelve una promesa con la que se recibe su valor.
Si la clave no existe, se obtiene `null` en **JavaScript** y `nil` en **Dogma**.

Si se desea, se puede fijar un nuevo `ttl`, en segundos, al mismo tiempo que se accede a la clave.
Es muy útil cuando el TTL de una entrada se debe actualizar cada vez que es accededida.

Ejemplo:

```
////////////////
// JavaScript //
////////////////
cache.get("mysession").then((val) => {
  //...
});

cache.get("mysession", {ttl: 60*30}).then((val) => {
  //...
});

#########
# Dogma #
#########
cache.get("mysession").then(fn(val?)
  #...
end)

cache.get("mysession", {ttl=60*30}).then(fn(val?)
  #...
end)
```

### Método cache.remove()

Para suprimir una clave de la caché, se utiliza el método `remove()`:

```
////////////////
// JavaScript //
////////////////
cache.remove(key:string) : Promise

#########
# Dogma #
#########
fn cache.remove(key:text) : promise
```

Ejemplo:

```
////////////////
// JavaScript //
////////////////
cache.remove("clave").then(() => {
  //...
});

#########
# Dogma #
#########
cache.remove("clave").then(fn()
  #...
end)
```

Si la clave no existe, no habrá error.
Simplemente, se omite la operación como si nada.
