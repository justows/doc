# Redis WebServices/Job

**Redis WebServices/Job** es un servidor de publicación de mensajes planificados representados mediante trabajos.
Tiene como objeto publicar mensajes en una o más colas:

- De manera periódica como, por ejemplo, una vez al día.
- En determinados momentos concretos como, por ejemplo, el primer día del año a las 12:35.

Sus componentes son los siguientes:

- Servidor de trabajos.
- Servicios de trabajos.
- Trabajos.

Este servidor, al igual que los demás, se organiza en servicios y, éstos a su vez, en trabajos y/o carpetas.

## Servidor de trabajos

El **servidor de trabajos** (*job server*) es el componente que se encarga de ejecutar los trabajos.
Se define mediante el campo `job` del objeto `servers` de la aplicación.
Por convenio y buenas prácticas, se recomienda su definición en un archivo dedicado:

- `job.js` si estamos usando **JavaScript**.
- `job.dog` cuando se usa **Dogma**.

Ejemplo:

```
////////////////
// JavaScript //
////////////////

// app.js
export default {
  name: "myapp",
  desc: "My test app.",
  version: "1.0.0",
  conn: require("./conn"),
  servers = {
    job: require("./job"),
    ...
  }
}

// job.js
export default {
  conn: {...},
  services: {
    puntoMontaje1: {
      //definición del servicio
    },

    puntoMontaje2: {
      //definición del servicio
    },

    ...
  }
}

#########
# Dogma #
#########

# app.dog
export {
  name = "myapp"
  desc = "My test app."
  version = "1.0.0"
  conn = use("./conn")
  servers = {
    job = use("./job")
    ...
  }
}

# job.dog
export {
  conn = {...}
  services = {
    puntoMontaje1 = {
      #definición del servicio
    }

    puntoMontaje2 = {
      #definición del servicio
    }

    ...
  }
}
```

El objeto de metadatos del servidor contiene los siguientes campos:

- `services` (objeto). Los servicios de trabajos que debe montar el servidor con sus definiciones.
- `conn` (objeto). Los conectores específicos del servidor.

## Servicios de trabajos

Un **servicio de trabajos** (*job service*) está orientado a trabajos y proporciona un conjunto de trabajos a añadir al servidor.
Se definen de manera similar a los demás servicios, teniendo en cuenta lo siguiente:

- Todo trabajo es un recurso de tipo archivo.
- Los trabajos cuelgan directa o indirectamente del recurso `home` del servicio.
- Cuando no se indica el campo `when`, se utiliza el nombre del recurso como el momento.
- Si un servicio debe utilizar un servidor de cola distinto al resto, se puede definir un objeto `q` específico.
- Los servicios se pueden definir como paquetes de **NPM**, lo más común, lo que facilita su reutilización, mantenimiento, desarrollo y prueba.
  En cuyo caso, hay que recordar que el paquete debe exportar el objeto de metadatos del servicio, al igual que sucede con los demás tipos de servicio.

Vamos a ver un ejemplo de objeto de metadatos de servicio:

```
////////////////
// JavaScript //
////////////////
export default {
  name: "nombre del servicio",
  desc: "descripción del servicio",
  version: "versión del servicio",
  conn: { ... },
  home: ...
}

#Dogma
export {
  name = "nombre del servicio"
  desc = "descripción del servicio"
  version = "versión del servicio"
  conn = { ... }
  home = ...
}
```

Recordemos que cada trabajo utilizará el objeto `q`.
Si el servicio necesita un servicio de mensajería distinto al resto, puede definir su propio `q`, recordemos, en el campo `conn`.

Recordemos que el recurso `home` representa la raíz del árbol de recursos del servicio.
Cada recurso puede ser un trabajo o una carpeta.
Las carpetas se identifican fácilmente mediante el campo `children` y representan contenedores lógicos de otras carpetas y/o trabajos.
Cuando un recurso no contiene el campo `children`, automáticamente se considera un trabajo.

## Trabajos

Un **trabajo** (*job*) es una operación de publicación planificada y se representa mediante un recurso (de tipo archivo) del servicio.
Los trabajos contienen los siguientes componentes:

- El momento en el que debe ejecutarse.
- Los mensajes a publicar.

Para ir abriendo boca, he aquí un ejemplo de definición de trabajo que se ejecutará cada hora:

```
////////////////
// JavaScript //
////////////////
trabajo: {
  when: ":00",
  publish: [
    {q: "una cola", msg: "un mensaje."},
    {q: "otra cola", msg: "otro mensaje."}
  ]
}

#########
# Dogma #
#########
trabajo = {
  when = ":00"
  publish = [
    {q="una cola", msg="un mensaje."}
    {q="otra cola", msg="otro mensaje."}
  ]
}
```

No olvidemos que también podemos indicar los campos `times`, `begin` y `end`.

### Momento de ejecución

Mediante el campo `when`, se indica en qué momento se debe ejecutar el trabajo, o sea, cuándo publicar los mensajes.
Consiste en un campo de texto que debe cumplir la siguiente sintaxis:

```
[on Día] [Mes] [Año] [Hora]:Minuto

Día = Número|Mon|Tue|Thu|Wed|Fri|Sat|Sun
Mes = Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec
Año = Cuatro dígitos
Hora = hora de dos dígitos entre 00 y 23
Minuto = minuto de dos dígitos entre 00 y 59 o bien *
```

He aquí algunos ejemplos ilustrativos:

- `:*`, en cada momento.
- `:15`, ejecutar cada hora en su minuto 15.
- `12:*`, ejecutar cada minuto de las 12.
- `12:34`, ejecutar a las 12:34 de cada día.
- `2018 12:34`, durante el año 2018 a las 12:34 de cada día.
- `Jan 12:34`, durante enero a las 12:34 de cada día.
- `Jan 2018 12:34`, durante enero de 2018 a las 12:34 de cada día.
- `on Mon 12:34`, los lunes a las 12:34.
- `on Mon Jan 12:34`, los lunes de enero a las 12:34.
- `on Mon Jan 2018 12:34`, los lunes de enero de 2018 a las 12:34.
- `on 1 2018 12:34`, el día 1 de cada mes de 2018 a las 12:34.

El comodín `*` como minuto representa cada minuto.
Este formato se puede usar para atender aquellas situaciones no representables por los anteriores.

### Mensaje(s) a publicar

Para indicar los mensajes a publicar se utiliza el campo `publish`, el cual puede ser un objeto o una lista de objetos, cada uno de los cuales representa un mensaje.
Cada mensaje debe contener los siguientes campos:

- `q` (texto). Nombre de la cola en la que publicar.
- `msg` (cualquiera). Mensaje a publicar.
  Cuando se publique se convertirá implícitamente a **JSON**: en **JavaScript**, usando `JSON.stringify()`; y en **Dogma**, mediante `json.encode()`.

He aquí unos ejemplos ilustrativos:

```
////////////////
// JavaScript //
////////////////
publish: {q: "la cola", msg: "el mensaje."}
publish: [{q: "una cola", msg: "un mensaje."}, {q: "otra cola", msg: "otro mensaje."}]

#########
# Dogma #
#########
publish = {q="la cola", msg="el mensaje."}
publish = [{q="una cola", msg="un mensaje."}, {q="otra cola", msg="otro mensaje."}]
```

A la hora de publicar los mensajes, el trabajo utilizará el objeto `q` definido en el servicio, el servidor de trabajos o en la aplicación.
Así pues, todos los trabajos definidos en el mismo servicio compartirán la misma cola, sea propia del servicio o definida a nivel del servidor o de la aplicación.

### Servicios dinámicos

Es posible configurar un servidor de trabajos como dinámico, aceptando nuevos trabajos de una cola.
Para ello, es necesario configurar la propiedad `dynamism` del servicio.

El campo `dynamism` es como sigue:

```
////////////////
// JavaScript //
////////////////
dynamism: {
  q: "cola",        //cola por la que recibir los mensajes
  enabled: true
}

#########
# Dogma #
#########
dynamism = {
  q = "cola"        #cola por la que recibir los mensajes
  enabled = true
}
```

Los mensajes recibidos a través de la cola indicada sólo se tienen en cuenta si son mensajes de texto en formato **JSON** que representan un mensaje como el siguiente:

```
{
  "type": "insert",
  "service": "punto de montaje del servicio",
  "job": trabajo
}
```

Los trabajos dinámicos se añadirán a la carpeta `dynamic` del recurso `home` del servicio indicado.

Ejemplos de mensajes dinámicos:

```
{
  "type": "insert",
  "service": "backups",
  "job": {
    "when": "on 1 Jan 00:00",
    "times": 1,
    "q": "backups",
    "msg", {"db": "*", "type": "full"}
  }
}

{
  "type": "insert",
  "service": "backups",
  "job": {
    "when": "on 1 Jan 00:00",
    "times": 1,
    "publish": {"q": "backups", msg: {"db": "*", "type": "full"}}
  }
}
```
