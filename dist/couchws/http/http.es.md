# Couch WebServices/HTTP

**Couch WebServices/HTTP** es un servidor de **HTTP**, desarrollado con el objeto de facilitar la creación de APIs **RESTs**.

Sus componentes son los siguientes:

- Servidor **HTTP**.
- Servicios **HTTP**.
- Recursos **HTTP**.

Está basado en funciones, facilitando el desarrollo de aplicaciones *serverless*, tal como hacen otros servidores como **Amazon Lambda**.

## Servidor HTTP

El **servidor HTTP** (*HTTP server*) es el componente que se encarga de servir los servicios **HTTP**.
Se define mediante el campo `http` del objeto `servers` de la aplicación.
Por convenio y buenas prácticas, se recomienda ubicar su definición en un archivo dedicado:

- `http.js` si estamos usando **JavaScript**.
- `http.dog` cuando se usa **Dogma**.

Ejemplo:

```
////////////////
// JavaScript //
////////////////

// app.js
export default {
  name: "myapp",
  desc: "My test app.",
  version: "1.0.0",
  conn: require("./conn"),
  servers = {
    http: require("./http"),
    ...
  }
}

// http.js
export default {
  endpoints: [...],
  conn: {...},
  services: {
    puntoMontaje1: {
      //definición del servicio
    },

    puntoMontaje2: {
      //definición del servicio
    },

    ...
  }
}

#########
# Dogma #
#########

# app.dog
export {
  name = "myapp"
  desc = "My test app."
  version = "1.0.0"
  conn = use("./conn")
  servers = {
    http = use("./http")
    ...
  }
}

# http.dog
export {
  endpoints = [...]
  conn = {...}
  services = {
    puntoMontaje1 = {
      #definición del servicio
    }

    puntoMontaje2 = {
      #definición del servicio
    }

    ...
  }
}
```

El objeto de metadatos del servidor contiene los siguientes campos:

- `endpoints` (lista u objeto). Extremos donde debe escuchar el servidor.
- `services` (objeto). Los servicios **HTTP** que debe montar el servidor con sus definiciones.
  Cada propiedad representa un servicio donde su nombre indica el punto de montaje; y el valor, la definición.
- `conn` (objeto). Si el servidor utiliza conectores específicos, independientes de los indicados a nivel de aplicación, se pueden definir en su propio `conn`.
  Éstos serán compartidos por todos los servicios **HTTP** a menos que definan otros explícitamente a nivel de servicio.

### Extremos

Para indicar los extremos donde debe escuchar el servidor **HTTP**, tenemos dos maneras.

Si sólo vamos a trabajar con **HTTP**, podemos utilizar una lista donde se indican los extremos en formato `http://host:puerto`.

La segunda opción es necesaria siempre que trabajemos con **HTTPS**.
En este caso, en vez de indicar una lista, usar un objeto donde cada campo hace referencia a un extremo.
El nombre o clave indica el extremo en formato `http://host:puerto` o `https://host:puerto`.
Por su parte, el valor es un objeto que contiene las opciones:

- `key` (texto). Archivo de la clave a usar.
  Ejemplo: `key.pem`.
- `cert` (texto). Archivo del certificado a usar.
  Ejemplo: `cert.pem`.

Vamos a ver unos ejemplos:

```
////////////////
// JavaScript //
////////////////
endpoints: ["http://localhost:80", "http://localhost:8080"]

endpoints: {
  "http://localhost:8080": {},
  "https://localhost:8888": {
    key: path.join(__dirname, "../key.pem"),
    cert: path.join(__dirname, "../cert.pem")
  }
}

#########
# Dogma #
#########
endpoints = ["http://localhost:80", "http://localhost:8080"]

endpoints = {
  'http://localhost:8080' = {}
  'https://localhost:8888' = {
    key = path.join(__dirname, "../key.pem")
    cert = path.join(__dirname, "../cert.pem")
  }
}
```

## Servicios HTTP

Un **servicio HTTP** (*HTTP service*) es un componente, orientado a recursos, que implementa o proporciona una determinada funcionalidad.
Los servicios son sin estado (*stateless*), lo que favorece su escalabilidad.
Y cada uno de ellos puede ser desarrollado por un equipo independiente, por ejemplo, mediante un paquete **NPM**.

Los servicios son montados por el servidor en los puntos de montaje indicados.
Así, por ejemplo, si el servidor está escuchando en `http://localhost:80` y montamos dos servicios en `m1` y `m2`, sus rutas de acceso serán `http://localhost:80/m1` y `http://localhost:80/m2`.

El servicio montado en `_dflt` se conoce formalmente como **servicio predeterminado** (*default service*) y es el que usará el servidor cuando el usuario indique una ruta que no corresponda a ningún punto de montaje.

Todo servicio está formado por recursos, los cuales se deben definir en su objeto de metadatos:

- `name` (texto). Nombre del servicio.
  Puede ser distinto al punto de montaje.
- `desc` (texto). Descripción del servicio.
- `version` (texto). versión del servicio.
- `home` (objeto). Representa el recurso raíz del servicio.
  De él, cuelgan los demás.
  Cada campo del objeto representa un recurso, ya sea una carpeta o un archivo.
- `conn` (objeto). Conectores específicos usados por el servicio.
- `status` (número). Código de estado predeterminado para aquellos casos que no se indique explícitamente.
- `contentType` (texto). `Content-Type` predeterminado para aquellos casos que no se indique explícitamente.

Ejemplo:

```
////////////////
// JavaScript //
////////////////

// http.js
export default {
  endpoints: ["http://localhost:8080"],
  services: {
    _dflt: {
      name: "ex1",
      desc: "The default service.",
      version: "1.0.0",
      home: {
        hello: (resp) => { resp.text("Hello world!") },
        bye: (resp) => { resp.text("Bye world!") }
      }
    }
  }
}


#########
# Dogma #
#########

# http.dog
export {
  endpoints = ["http://localhost:8080"]
  services = {
    _dflt = {
      name = "ex1"
      desc = "The default service."
      version = "1.0.0"
      home = {
        hello = fn(resp) resp.text("Hello world!") end
        bye = fn(resp) resp.text("Bye world!") end
      }
    }
  }
}
```

En este ejemplo, el servidor define un único servicio, el predeterminado.
Cuando el usuario acceda a `http://localhost:8080/hello`, el servidor responderá con un `text/plain` con contenido *Hello world!*.
Y cuando acceda a `http://localhost:8080/bye`, el servidor responderá con `text/plain` con cuerpo *Bye world!*.

## Recursos

Un **recurso** (*resource*) representa un objeto accesible por **HTTP**.
Se distingue entre **carpetas** (*folders*), aquellas que agrupan recursos; y **archivos** (*files*), aquellos que representan elementos como imágenes, métodos webs, etc.

El recurso raíz lo representa el campo `home` del servicio y *siempre* es una carpeta.
Cada uno de sus elementos representa un recurso.
Aquellos que contienen el campo `children` se consideran carpetas, mientras que los que no lo hacen, como archivos.

Recordemos, de **HTTP**, que un recurso puede ser accedido usando distintos métodos: `GET`, `PUT`, `POST`, `HEAD`, etc.

Cuando se usa un método, el servidor lo que hace es:

1. Buscar el servicio entre los montados.
2. Buscar el recurso en el servicio correspondiente.
3. Ejecutar las acciones del recurso.

Recordemos que las acciones se ejecutan en el siguiente orden:

1. Preacciones.
2. Acción del método solicitado por el usuario: `GET`, `PUT`...
3. Postacciones.

Las preacciones se definen mediante `pre`; las postacciones mediante `post`; y los métodos mediante `GET`, `PUT`, etc.
Vamos a ver un ejemplo:

```
////////////////
// JavaScript //
////////////////
hello: {
  GET: (req, resp) => {
    //...
  },

  PUT: (req, resp) => {
    //...
  }
}

#########
# Dogma #
#########
hello = {
  GET = fn(req, resp)
    #...
  end

  PUT = fn(req, resp)
    #...
  end
}
```

Sintaxis de un recurso de tipo carpeta:

```
////////////////
// JavaScript //
////////////////
nombre: {
  pre: preacciones,
  acción1: ...,
  acción2: ...,
  post: postacciones,
  children: {
    descendientes
  }
}

#########
# Dogma #
#########
nombre = {
  pre = preacciones
  acción1 = ...
  acción2 = ...
  post = postacciones
  children = {
    descendientes
  }
}
```

Mientras que para definir un archivo, hay que usar la sintaxis anterior, pero sin indicar `children`:

```
////////////////
// JavaScript //
////////////////
nombre: {
  pre: preacciones,
  acción1: ...,
  acción2: ...,
  post: postacciones
}

#########
# Dogma #
#########
nombre = {
  pre = preacciones
  acción1 = ...
  acción2 = ...
  post = postacciones
}
```

Si el archivo sólo define la acción `GET`, se puede indicar directamente.
Ejemplo:

```
////////////////
// JavaScript //
////////////////
hello: (req, resp) => {
  //...
}

#########
# Dogma #
#########
hello = fn(req, resp)
  #...
end
```

### Parámetros

Un recurso puede contener parámetros, permitiendo así adaptar su ruta.
Cada parámetro se indica mediante `&nombre` o `&nombre:tipo`.
Donde `tipo` puede ser `text`, `num` o `bool`.
Cuando se indica un tipo, se convertirá automáticamente el valor pasado por el usuario al tipo indicado.
El tipo predeterminado es `text`.
Ejemplo:

```
////////////////
// JavaScript //
////////////////
"usuario/&id:num": (params, db, done) => {
  //params.id contiene el valor indicado por el usuario como número
}

"usuario/&id": (params, db, done) => {
  //params.id contiene el valor indicado por el usuario como texto
}

#########
# Dogma #
#########
'usuario/&id:num' = fn(params, db, done)
  #params.id contiene el valor indicado por el usuario como número
end

'usuario/&id' = fn(params, db, done)
  #params.id contiene el valor indicado por el usuario como texto
end
```

Los parámetros se encuentran disponibles en el objeto `req.params` o simplemente mediante el parámetro inyectable `params`.

## Acciones

Vamos a hacer un poco más de hincapié en las acciones.
Un **acción** (*action*) no es más que una operación, implementada mediante una función, con la que hacer alguna cosa.
**Couch WebServices** se ha implementado mediante un sistema de acciones con nombre.
Se puede asignar varias acciones al mismo recurso sin problemas.

Una acción puede tener cero, una o más funciones asociadas.
Cuando se desea indicar varias, introducirlas en una lista.

En el caso de **HTTP**, el servidor buscará la acción cuyo nombre es el del método solicitado por el usuario.
Así pues, tal como hemos visto anteriormente, para indicar la acción para el método `GET`, no hay más que definir una acción `GET`.
El uso de mayúsculas y minúsculas en los nombres es importante.
En **HTTP**, la acción `post` hace referencia a una postacción; mientras que `POST`, a la acción a ejecutar cuando se reciba un mensaje con método `POST`.

Por otra parte, tenemos las preacciones, aquellas cuyo nombre es `pre`.
Éstas se ejecutan antes de la acción del método web.
Son muy útiles para realizar inicializaciones, comprobaciones de autorización, etc.
Se pueden definir a nivel de archivo, carpeta, servicio, servidor y aplicación.

Mientras que las postacciones, aquellas que se ejecutan tras la acción del método web, se nombran como `post`.

Cuando el usuario accede a un recurso, el servidor trabaja como sigue:

1. Busca el servicio.
2. Busca el recurso.
3. Ejecuta las preacciones: aplicación, servidor, servicio, carpetas y recurso.
4. Ejecuta la acción del método web: GET, PUT, POST, etc.
5. Ejecuta las postacciones: recurso, carpetas, servicio, servidor y aplicación.

Recordemos que las acciones pueden tener parámetros, los cuales se inyectan por nombre.
Sólo debemos indicar aquellos objetos que necesitemos.
Estando disponibles los siguientes:

- `db`, objeto para acceder a los datos.
- `q`, objeto con el que publicar mensajes en el servidor de colas.
- `cache`, objeto para acceder a la caché de la aplicación.
- `logger`, objeto de *logging*.
- `req`, objeto que representa la solicitud HTTP remitida por el usuario.
- `params`, objeto que contiene los parámetros de la ruta.
- `resp`, objeto que representa la respuesta HTTP a enviar al usuario.
- `done`, función a ejecutar si la acción es asíncrona para indicar su finalización.
  Si no se indica, la función se procesa de manera síncrona.

### Objeto req

Mediante el parámetro nominal `req`, se solicita al servidor que inyecte el objeto que representa la petición **HTTP** bajo procesamiento.
Cuyos campos son los siguientes:

- `http` (objeto). Información de **HTTP**:

  - `scheme` (texto). Protocolo: `http` o `https`.
  - `method` (texto). Método web: `GET`, `PUT`, `POST`, etc.
  - `version` (texto). Versión de **HTTP**.

- `client` (objeto). Información del cliente:
  - `addr` (texto). Dirección de IP.
  - `port` (número). Puerto.
  - `browser` (texto). Navegador.

- `headers` (objeto). Cabeceras de **HTTP**:
  - `headers.get(nombre) : valor` devuelve el valor de una cabecera. Si no existe, devuelve nulo.
  - `headers.set(nombre, valor) : Headers` fija el valor de una cabecera. Si ya existe, la sobrescribe.
  - `headers.remove(nombre) : Headers` suprime una cabecera. Si no existe, no hace nada.

- `cookies` (objeto). *Cookies*:
  - `cookies.get(nombre) : valor` devuelve el valor de una *cookie*. Si no existe, devuelve nulo.
  - `cookies.set(nombre, valor, opciones) : Cookies` fija el valor de una *cookie*. Si ya existe, la sobrescribe.
  - `cookies.remove(nombre) : Cookies` suprime una *cookie*. Si no existe, no hace nada.

- `params` (objeto). Los valores de los parámetros de la ruta.

- `content() : cualquiera`. Devuelve el contenido.
  Cuando el cuerpo es `application/json` o `application/x-www-form-urlencoded`, devuelve un objeto.

### Objeto resp

Mediante el parámetro nominal `resp`, las acciones pueden acceder al objeto con el que determinar el mensaje a enviar al usuario.
Sus miembros son:

- `headers` (objeto). Similar al de `req`.
- `cookies` (objeto). Similar al de `req`.
- `status() : número`. Devuelve el código de estado.
- `status(code) : resp`. Fija el código de estado.
- `contentType() : texto`. Devuelve `Content-Type`.
- `contentType(cType) : resp`. Fija `Content-Type`.
- `content() : cualquiera`. Devuelve el contenido.
- `content(c) : resp`. Fija el contenido.
- `text(c) : resp`. Fija el contenido y asigna `text/plain` como tipo de contenido.
- `html(c) : resp`. Fija el contenido y asigna `text/html` como tipo de contenido.
- `json(c) : resp`. Fija el contenido y asigna `application/json` como tipo de contenido.
- `redirectTo(uri) : resp`. Devuelve un mensaje de redireccionamiento al URI indicado, con código de estado 302, si otro no se ha fijado ya explícitamente.
- `redirectTo(uri, status) : resp`. Devuelve un mensaje de redireccionamiento al URI indicado, con el código de estado indicado.
