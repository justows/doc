# Couch WebServices/QPub

**Couch WebServices/QPub** es la especificación que describe la API utilizada por el parámetro `q` de las acciones.
Tiene como objeto describir cómo publicar mensajes mediante este objeto.

## Implementaciones

En **Couch WebServices**, se utiliza un servidor **Redis** como sistema de mensajería.
Durante el tercer trimestre de 2018, también se podrá usar **Kafka**.

### Uso de una implementación

Recordemos que el parámetro `q` representa el objeto con el que las acciones pueden publicar mensajes en una o más colas de un servidor.
Se puede configurar:

- A nivel de aplicación, en su objeto de metadatos.
- A nivel de servidor de servicios.
- A nivel de servicio.

La configuración debe contener las propiedades de conexión al servidor de colas.

Ejemplo:

```
////////////////
// JavaScript //
////////////////
conn: {
  q: {
    host: "localhost",
    port: 6379,
    db: 0
  }
}

#########
# Dogma #
#########
conn = {
  q = {
    host = "localhost"
    port = 6379
    db = 0
  }
}
```

## Parámetro q

El parámetro `q` de las acciones presenta un único método, `publish()`, con el que publicar un mensaje en una cola:

```
q.publish(msg:any, queue:text) : promise
```

El método puede leerse como sigue: *publica el mensaje M en la cola C*.

Los mensajes se publican en texto formateado a **JSON** mediante una llamada implícita a `JSON.stringify()` en **JavaScript** o `json.encode()` en **Dogma**.

Ejemplo:

```
//JavaScript
(q, done) => {
  q.publish({to: "equipo", text: "Buenos días!!!"}, "email").then(done, done);
}

//Dogma
fn(q, done)
  q.publish({to="equipo", text="Buenos días!!!"}, "email").then(done, done)
end
```
