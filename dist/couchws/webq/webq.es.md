# Couch WebServices/WebQ

**Couch WebServices/WebQ** es un servidor de colas accesible mediante **WebSocket**.
Tiene como objeto proporcionar el consumo de datos en tiempo real demandado por las aplicaciones de hoy en día.

Sus componentes son los siguientes:

- Servidor de colas webs.
- Servicios de colas webs.
- Autorizador de acceso a cola.

La estructura es similar a la de **HTTP**, pero las acciones representan autorizadores.

## Servidor de colas webs

El **servidor de colas webs** (*web queue server*) actúa como un *proxy* de colas que permite publicar y consumir mensajes de colas.
Se define mediante el campo `webQ` del objeto `servers` de la aplicación.
Por convenio y buenas prácticas, se recomienda ubicar su definición en un archivo dedicado:

- `webQ.js` si estamos usando **JavaScript**.
- `webQ.dog` cuando se usa **Dogma**.

Ejemplo:

```
////////////////
// JavaScript //
////////////////

// app.js
export default {
  name: "myapp",
  desc: "My test app.",
  version: "1.0.0",
  conn: require("./conn"),
  servers = {
    webQ: require("./webQ"),
    ...
  }
}

// webQ.js
export default {
  endpoints: [...],
  conn: {...},
  services: {
    puntoMontaje1: {
      //definición del servicio
    },

    puntoMontaje2: {
      //definición del servicio
    },

    ...
  }
}

#########
# Dogma #
#########

# app.dog
export {
  name = "myapp"
  desc = "My test app."
  version = "1.0.0"
  conn = use("./conn")
  servers = {
    webQ = use("./webQ")
    ...
  }
}

# webQ.dog
export {
  endpoints = [...]
  conn = {...}
  services = {
    puntoMontaje1 = {
      #definición del servicio
    }

    puntoMontaje2 = {
      #definición del servicio
    }

    ...
  }
}
```

El objeto de metadatos del servidor contiene los siguientes campos:

- `endpoints` (lista). Lista de extremos donde debe escuchar el servidor.
  Formato: `ws://servidor:puerto`.
- `services` (objeto). Los servicios de colas que debe montar el servidor con sus definiciones.
  Cada propiedad representa un servicio donde su nombre indica el punto de montaje; y el valor, la definición.
- `conn` (objeto). Si el servidor utiliza conectores específicos, independientes de la aplicación, se pueden definir en su propio `conn`.
  Éstos serán compartidos por todos los servicios de colas webs a menos que definan otros explícitamente.
  Generalmente, se define un objeto `q` específico a nivel de servidor o servicio.

## Servicios de colas webs

Un **servicio de colas webs** (*web queue service*) es un componente, orientado a recursos de tipo cola, que implementa o proporciona acceso a determinadas colas mediante **WebSocket**.
Estos servicios definen las colas que son accesibles e indican las funciones con las que llevar a cabo la autorización de acceso.

Todo servicio está formado por colas.
Y su objeto de metadatos es como sigue:

- `name` (texto). Nombre del servicio.
  Puede ser distinto al punto de montaje.
- `desc` (texto). Descripción del servicio.
- `version` (texto). Versión del servicio.
- `home` (objeto). Representa el recurso raíz del servicio.
  De él, cuelgan los demás.
  Cada campo del objeto representa un recurso, ya sea una carpeta o un archivo (o sea, una cola).
- `conn` (objeto). Conectores específicos usados por el servicio.

Ejemplo:

```
////////////////
// JavaScript //
////////////////
export default {
  endpoints: ["ws://localhost:8080"],
  conn: {
    q: {
      host: "localhost",
      port: 6379,
      db: 0
    }
  },
  services: {
    one: {
      home: {
        cola: (msg, req, done) => {
          //...
        },

        cola2: (msg, req, done) => {
          //...
        }
      }
    }
  }
}

#########
# Dogma #
#########
export {
  endpoints = ["ws://localhost:8080"]
  conn = {
    q = {
      host = "localhost"
      port = 6379
      db = 0
    }
  }
  services = {
    one = {
      home = {
        cola = fn(msg, req, done)
          #...
        end

        cola2 = fn(msg, req, done)
          #...
        end
      }
    }
  }
}
```

## Recursos

Las colas se representan como recursos de tipo archivo, siendo posible utilizar también carpetas.
En el caso de una cola, el nombre del recurso indica la cola.
No podemos definir varias colas con el mismo nombre dentro del mismo servicio, ni aún en carpetas distintas.

La cola debe definir como acción `auth`, la acción que debe ejecutar el servidor para comprobar si el usuario tiene permiso para ejecutar la operación.
Si se define el recurso como una función, ésta será su acción `auth`.
Los dos ejemplos siguientes son similares:

```
////////////////
// JavaScript //
////////////////
cola: (req, msg, db) => {
  //...
}

cola: {
  auth: (req, msg, db) => {
    //...
  }
}

#########
# Dogma #
#########
cola = fn(req, msg, db)
  #...
end

cola = {
  auth = fn(req, msg, db)
    #...
  end
}
```

### Acciones

Las acción `auth` se conoce formalmente como **autorizador** (*authorizer*) y se ejecuta cada vez que se recibe un mensaje por **WebSocket**.
La acción puede solicitar la inyección de los siguientes objetos:

- `db`.
- `q`.
- `cache`.
- `logger`.
- `req` (objeto), solicitud **HTTP** con la que el usuario abrió la conexión **WebSocket**.
- `msg` (texto), mensaje bajo procesamiento.
- `done`, función a ejecutar si la acción es asíncrona una vez finalizada.
  Si no se indica, la función se procesa de manera síncrona.

La función tiene dos maneras de denegar un acceso:

- Si es síncrona, propagando un error.
  Si la función acaba sin error, el servidor considera que el usuario tiene acceso.
- Si es asíncrona, pasando el error como primer argumento de `done()`.
  Si no se pasa nada, el servidor considera que el usuario tiene acceso.

El objeto `req` es como sigue:

- `http` (objeto). Información de **HTTP**:
  - `scheme` (texto). Protocolo: `http`.
  - `method` (texto). Método web.
  - `version` (texto). Versión de **HTTP**.

- `client` (objeto). Información del cliente:
  - `addr` (texto). Dirección de IP.
  - `port` (número). Puerto.
  - `browser` (texto). Navegador.

- `headers` (objeto). Cabeceras de **HTTP**:
  - `headers.get(nombre) : valor` devuelve la cabecera indicada.

- `cookies` (objeto). *Cookies*:
  - `get(nombre) : valor` devuelve la *cookie* indicada.

### Mensajes recibidos por WebSocket

A través de la conexión **WebSocket**, el usuario puede remitir los siguientes mensajes:

- `SUB servicio cola`, suscribir a la cola del servicio indicado.
- `UNSUB servicio cola`, dar de baja de la cola del servicio indicado.
- `PUB mensaje servicio cola`, publicar el mensaje en la cola del servicio indicado.

Cada vez que se recibe un mensaje, el servidor actúa como sigue:

1. Busca el servicio montado, usando el punto de montaje y el servicio indicado por el usuario.
2. Busca la cola entre sus recursos.
3. Ejecuta las preacciones: aplicación, servidor, servicio, carpetas, cola.
4. Ejecuta el autorizador de la cola, esto es, la acción `auth`.
5. Si el autorizador no propaga error, pasa a ejecutar el comando `PUB`, `SUB` o `UNSUB`.
6. Ejecuta las postacciones: cola, carpetas, servicio, servidor, aplicación.

El servidor responde con `OK` si la operación se ha llevado a cabo correctamente.
En otro caso, con `ERR mensaje de error`.

Los mensajes publicados mediante `PUB` deben encontrarse en formato **JSON**.

El servidor utiliza el objeto `q` para suscribirse a las colas y para publicar.
De ahí que generalmente se utilice un `q` específico a nivel de servidor o servicio.

Cuando un cliente cierra la conexión, se ejecuta un `UNSUB` implícito sobre todas sus colas.
Pero ojo, no se ejecuta contra el sistema de mensajería a menos que sea el único que está suscrito.

### Mensajes recibidos por las colas

Cada vez que un usuario envía un mensaje `SUB`, si el autorizador no lo impide, el servidor se suscribe a la cola indicada.
De tal manera que cuando el servidor recibe un mensaje de esta cola, automáticamente se lo envía al usuario mediante la conexión **WebSocket** abierta.
Para ello, le envía un mensaje como el siguiente:

```
MSG mensaje
```

El mensaje recibido y remitido al usuario debe encontrarse en formato **JSON**.
Ejemplo:

```
MSG '{"band":"Kaiser Chiefs","origin":"UK"}'
```

Si son varios los clientes suscritos a la cola, el mensaje se lo remitirá a todos ellos.
Así es como se puede tener acceso en tiempo real a las modificaciones de los datos.
Suscribiendo las aplicaciones webs a las colas correspondientes mediante **WebSocket** y publicando los cambios en las colas correspondientes.

Por ejemplo, si tiene una aplicación que muestra noticias, puede usar una cola `noticias` a la que remitir cualquier nueva noticia.
A su vez, haga que sus clientes se suscriban a la cola mediante **WebSocket** y así podrán recibir las nuevas publicadas para presentárselas a los usuarios.
